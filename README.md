This repository contains an example experiment for PsychoPy with stimuli.

Download the latest version of PsychoPy at:
https://www.psychopy.org

For a test run, open the `.psyexp` file in PsychoPy and press `Run Experiment` in the Builder view.
